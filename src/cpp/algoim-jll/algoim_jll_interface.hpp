#ifndef ALGOIM_JLL_INTERFACE_HPP
#define ALGOIM_JLL_INTERFACE_HPP

// #include <vector>
// #include "blitz/array.h"
#include "algoim/algoim_quad.hpp"

using Order = int;

enum QType { Domain_2D=-1, Domain_3D=-1, NormalX=0, NormalY=1, NormalZ=2, Boundary_2D=2, Boundary_3D=3};

enum Side{Dummy=-1, Left=0, Right=1};

struct QuadRule2D{

    int n;
    Algoim::QuadratureRule<2> m_qrule;

    template<typename F> 
    QuadRule2D(const F& phi, Order qo, int dim, int side, double xa, double ya, double xb, double yb){

        // initialize
        blitz::TinyVector<double,2> c1{xa, ya};
        blitz::TinyVector<double,2> c2{xb, yb};
        Algoim::BoundingBox<double,2> bounding_box{c1, c2};
        
        // compute rule
        m_qrule = quadGen(phi, bounding_box, dim, side, qo);

        // determine dimensions
        n = m_qrule.nodes.size();
    }

    const int get_size() const { return n; }
    double* get_node(int i) { return &m_qrule.nodes[i].x(0); };

};


struct QuadRule3D{

    int n;
    Algoim::QuadratureRule<3> m_qrule;

    template<typename F> 
    QuadRule3D(const F& phi, Order qo, int dim, int side, double xa, double ya, double za, double xb, double yb, double zb){

        // initialize
        blitz::TinyVector<double,3> c1{xa, ya, za};
        blitz::TinyVector<double,3> c2{xb, yb, zb};
        Algoim::BoundingBox<double,3> bounding_box{c1, c2};
        
        // compute rule
        m_qrule = quadGen(phi, bounding_box, dim, side, qo);

        // determine dimensions
        n = m_qrule.nodes.size();
    }

    const int get_size() const { return n; }
    double* get_node(int i) { return &m_qrule.nodes[i].x(0); };

};

#endif