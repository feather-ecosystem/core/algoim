using Libdl, Cxx

path_to_root = joinpath(dirname(Base.source_path()),"../")

# # external c++ header paths
addHeaderDir(joinpath(path_to_root, "src/cpp"), kind=C_System)
addHeaderDir(joinpath(path_to_root, "external/blitz-install/include"), kind=C_System)
addHeaderDir(joinpath(path_to_root, "external/algoim-install/include"), kind=C_System)

# c++ dll's to include in project
Libdl.dlopen(joinpath(path_to_root, "external/blitz-install/lib", "libblitz.so"), Libdl.RTLD_GLOBAL)
Libdl.dlopen(joinpath(path_to_root, "build/lib", "libalgoim-jll.so"), Libdl.RTLD_GLOBAL)

# main c++ header include files
cxxinclude("algoim-jll/algoim_jll_interface.hpp")