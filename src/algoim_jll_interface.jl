export AlgoimMapping, AlgoimQuadRule

struct AlgoimMapping{Dim,CxxMap}
    cxx_map::CxxMap
    function AlgoimMapping{Dim}(map::CxxMap) where {Dim,CxxMap}
        return new{Dim,CxxMap}(map)
    end
end

"""
    AlgoimQuadRule(map::AlgoimMapping, xa::Real, ya::Real, xb::Real, yb::Real, qo::Int64)

Compute a algoim quadrature rule in bounding box [xa, ya] × [xb, yb] based on a Gauss-Legendre
rule of `qo` points.
"""
const AlgoimViewType{T} = SubArray{T,1,Array{T,1},Tuple{StepRange{Int64,Int64}},true}

struct AlgoimQuadRule{Dim,T<:Real}
    x::NTuple{Dim,AlgoimViewType{T}}
    w::AlgoimViewType{T}

    function AlgoimQuadRule(map::AlgoimMapping{2}, x, y; order::Int64, dim::Int64=-1, side::Int64=-1)
        qr   = @cxxnew QuadRule2D(map.cxx_map, order, dim, side, x.a, y.a, x.b, y.b);
        ptr  = @cxx(qr->get_node(0))
        npts = Int64(@cxx(qr->get_size()))
        data = unsafe_wrap(Array, ptr, (3, npts); own = true)
        T = eltype(data)
        x = (view(data, 1:3:length(data)), view(data, 2:3:length(data)))
        w = view(data, 3:3:length(data))
        return new{2,T}(x, w) 
    end

    function AlgoimQuadRule(map::AlgoimMapping{3}, x, y, z; order::Int64, dim::Int64=-1, side::Int64=-1)
        qr   = @cxxnew QuadRule3D(map.cxx_map, order, dim, side, x.a, y.a, z.a, x.b, y.b, z.b);
        ptr  = @cxx(qr->get_node(0))
        npts = Int64(@cxx(qr->get_size()))
        data = unsafe_wrap(Array, ptr, (4, npts); own = true)
        T = eltype(data)
        x = (view(data, 1:4:length(data)), view(data, 2:4:length(data)), view(data, 3:4:length(data)))
        w = view(data, 4:4:length(data))
        return new{3,T}(x, w) 
    end
end

Base.length(qr::AlgoimQuadRule) = Base.length(qr.w)
Base.size(qr::AlgoimQuadRule) = (length(qr),)