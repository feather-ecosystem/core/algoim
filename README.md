# Algoim.jl

This package interfaces Julia with the c++ library [Algoim](https://algoim.github.io/), which implements algorithms for implicitly defined geometry.

## Installation
This package requires [Cxx.jl](https://github.com/JuliaInterop/Cxx.jl) which is only supported to julia version 1.3.1. Hence, this package can only be used with an old version of Julia and is tested only using julia 1.3.1 on linux. You also need a recent clang++ compiler in order to build this package. 

In order to build and use this package do the following
```
(v1.3) pkg> add Algoim
```
Then from your home folder go to `.julia/packages/Algoim/` and go to the respective `Algoim` package that you want to install. Next
```
mkdir build
cd build
cmake .. -DCMAKE_CXX_COMPILER=clang++ -DBUILD_TESTING=ON
cmake --build . --config Release
ctest
cmake --install .
```
If the library is correctly build then the tests should pass. Make sure you install within the build folder. 

Before you use this package within Julia make sure to add the following line to your `.bashrc` or `.bash_profile`
```
export JULIA_CXX_RTTI=1
```
Without it Julia will throw an error duting compilation of the package. If you run into an issue that julia misses version `GLIBCXX_3.4.29` (or similar) of `libstdc++` then you will have to manually replace `libstdc++` in the julia directory. You can contact one of the developers of this package in case that happens and we will assist you.

You are now ready to use the Julia package. See the tests for examples on how to call the quadrature routines.
