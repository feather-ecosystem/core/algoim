using Algoim        

package_info = Dict(
    "modules" => [Algoim],
    "authors" => "Rene Hiemstra and contributors",
    "name" => "Algoim.jl",
    "repo" => "https://gitlab.com/feather-ecosystem/core/Algoim",
    "pages" => [
        "About"  =>  "index.md"
        "API"  =>  "api.md"
    ],
)

DocMeta.setdocmeta!(Algoim, :DocTestSetup, :(using Algoim); recursive=true)

# if docs are built for deployment, fix the doctests
# which fail due to round off errors...
if haskey(ENV, "DOC_TEST_DEPLOY") && ENV["DOC_TEST_DEPLOY"] == "yes"
    doctest(Algoim, fix=true)
end
