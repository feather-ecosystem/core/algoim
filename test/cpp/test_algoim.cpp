#include <cmath>

#include "blitz/array.h"
#include "algoim/algoim_quad.hpp"
#include "test.h"


namespace algoim_test {

    template<int N>
    struct Ellipsoid
    {
        template<typename T>
        T operator() (const blitz::TinyVector<T, N>& x) const
        {
            if (N == 2)
                return x(0) * x(0) + 4.0 * x(1) * x(1) - 1.0;
            else
                return x(0) * x(0) + 4.0 * x(1) * x(1) + 9.0 * x(2) * x(2) - 1.0;
        }

        template<typename T>
        blitz::TinyVector<T, N> grad(const blitz::TinyVector<T, N>& x) const
        {
            if (N == 2)
                return blitz::TinyVector<T, N>(2.0 * x(0), 8.0 * x(1));
            else
                return blitz::TinyVector<T, N>(2.0 * x(0), 8.0 * x(1), 18.0 * x(2));
        }

    };

    // "Area of a 2D ellipse using automatic subdivision
    TEST(algoim, ellipse_2d_area_subdivision) {
        Ellipsoid<2> phi;
        Algoim::QuadratureRule<2> q = Algoim::quadGen<2>(phi, Algoim::BoundingBox<double, 2>(-1.1, 1.1), -1, -1, 4);
        double area = q([](const auto& x) { return 1.0; });
        EXPECT_TRUE(std::abs(area - 1.5707963267948966) < 1e-4);
    }

    // Volume of a 3D ellipsoid using automatic subdivision
    TEST(algoim, ellipse_3d_volume_subdivision) {
        Ellipsoid<3> phi;
        auto q = Algoim::quadGen<3>(phi, Algoim::BoundingBox<double, 3>(-1.1, 1.1), -1, -1, 4);
        double volume = q([](const auto& x) { return 1.0; });
        EXPECT_TRUE(std::abs(volume - 0.6981317007977318) < 1e-4);
    }

    // Area of a 2D ellipse, computed via the cells of a Cartesian grid
    TEST(algoim, ellipse_2d_area_fixed_mesh) {
        int n = 16;
        double dx = 2.2 / n;
        Ellipsoid<2> phi;
        double area = 0.0;
        for (int i = 0; i < n; ++i) for (int j = 0; j < n; ++j)
        {
            blitz::TinyVector<double, 2> xmin = { -1.1 + i * dx, -1.1 + j * dx };
            blitz::TinyVector<double, 2> xmax = { -1.1 + i * dx + dx, -1.1 + j * dx + dx };
            area += Algoim::quadGen<2>(phi, Algoim::BoundingBox<double, 2>(xmin, xmax), -1, -1, 4).sumWeights();
        }
        EXPECT_TRUE(std::abs(area - 1.5707963267948966) < 1e-4);
    }

    // Surface area of a 3D ellipsoid using automatic subdivision
    TEST(algoim, ellipse_3d_area_subdivision) {
        Ellipsoid<3> phi;
        auto q = Algoim::quadGen<3>(phi, Algoim::BoundingBox<double, 3>(-1.1, 1.1), 3, -1, 4);
        double surface_area = q.sumWeights();
        EXPECT_TRUE(std::abs(surface_area - 4.4008095646649703) < 1e-3);
    }

}