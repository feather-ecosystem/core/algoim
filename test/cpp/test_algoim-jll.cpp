#include <cmath>

#include "blitz/array.h"
#include "algoim-jll/algoim_jll_interface.hpp"
#include "test.h"


namespace algoim_test {

    struct UnitCircle
    {
        template<typename T>
        T operator() (const blitz::TinyVector<T, 2>& x) const
        {
            return x(0) * x(0) + x(1) * x(1) - 1.0;
        }

        template<typename T>
        blitz::TinyVector<T, 2> grad(const blitz::TinyVector<T, 2>& x) const
        {
            return blitz::TinyVector<T, 2>(2.0 * x(0), 2.0 * x(1));
        }
    };

    struct Ellipse
    {
        template<typename T>
        T operator() (const blitz::TinyVector<T, 2>& x) const
        {
            return x(0) * x(0) + 4.0 * x(1) * x(1) - 1.0;
        }

        template<typename T>
        blitz::TinyVector<T, 2> grad(const blitz::TinyVector<T, 2>& x) const
        {
            return blitz::TinyVector<T, 2>(2.0 * x(0), 8.0 * x(1));
        }
    };

    struct UnitSphere
    {
        template<typename T>
        T operator() (const blitz::TinyVector<T, 3>& x) const
        {
            return x(0) * x(0) + x(1) * x(1) + x(2) * x(2) - 1.0;
        }

        template<typename T>
        blitz::TinyVector<T, 3> grad(const blitz::TinyVector<T, 3>& x) const
        {
            return blitz::TinyVector<T, 3>(2.0 * x(0), 2.0 * x(1), 2.0 * x(2));
        }
    };

    // Area of a 2D ellipse, computed via the cells of a Cartesian grid
    TEST(algoim, ellipse_2d_area_fixed_mesh) {
        int n = 16;
        double dx = 2.2 / n;
        Ellipse phi;
        double area = 0.0;
        for (int i = 0; i < n; ++i) for (int j = 0; j < n; ++j)
        {
            auto Q = QuadRule2D(phi, Order(4), QType::Domain_2D, Side::Dummy, -1.1 + i * dx, -1.1 + j * dx, -1.1 + i * dx + dx, -1.1 + j * dx + dx);
            area += Q.m_qrule.sumWeights();
        }
        EXPECT_TRUE(std::abs(area - 1.5707963267948966) < 1e-4);
    }

    // perimeter of a Unit circle, computed via the cells of a Cartesian grid
    TEST(algoim, circle_perimeter_fixed_mesh) {
        int n = 16;
        double dx = 2.2 / n;
        UnitCircle phi;
        double length = 0.0;
        for (int i = 0; i < n; ++i) for (int j = 0; j < n; ++j)
        {
            auto Q = QuadRule2D(phi, Order(4), QType::Boundary_2D, Side::Dummy, -1.1 + i * dx, -1.1 + j * dx, -1.1 + i * dx + dx, -1.1 + j * dx + dx);
            length += Q.m_qrule.sumWeights();
        }
        EXPECT_TRUE(std::abs(length - 2 *  3.1415926535897) < 1e-4);
    }

    // perimeter of a Unit circle, computed via the cells of a Cartesian grid
    TEST(algoim, circle_side_fixed_mesh) {
        int n = 8;
        double dx = 1.2 / n;
        UnitCircle phi;
        double length = 0.0;
        for (int j = 0; j < n; ++j)
        {
            auto Q = QuadRule2D(phi, Order(4), QType::NormalX, Side::Left, 0.0, j * dx, dx, j * dx + dx);
            length += Q.m_qrule.sumWeights();
        }
        EXPECT_TRUE(std::abs(length - 1.0) < 1e-4);
    }

    // Volume of a Sphere, computed via the cells of a Cartesian grid
    TEST(algoim, sphere_3d_volume_fixed_mesh) {
        int n = 16;
        double dx = 2.2 / n;
        UnitSphere phi;
        double volume = 0.0;
        for (int i = 0; i < n; ++i) for (int j = 0; j < n; ++j) for (int k = 0; k < n; ++k)
        {
            auto Q = QuadRule3D(phi, Order(4), QType::Domain_3D, Side::Dummy, -1.1 + i * dx, -1.1 + j * dx, -1.1 + k * dx, -1.1 + i * dx + dx, -1.1 + j * dx + dx, -1.1 + k * dx + dx);
            volume += Q.m_qrule.sumWeights();
        }
        EXPECT_TRUE(std::abs(volume - (4.0/3.0) *  3.1415926535897) < 1e-4);
    }

    // Area of a sphera, computed via the cells of a Cartesian grid
    TEST(algoim, sphere_3d_surface_fixed_mesh) {
        int n = 16;
        double dx = 2.2 / n;
        UnitSphere phi;
        double area = 0.0;
        for (int i = 0; i < n; ++i) for (int j = 0; j < n; ++j) for (int k = 0; k < n; ++k)
        {
            auto Q = QuadRule3D(phi, Order(4), QType::Boundary_3D, Side::Dummy, -1.1 + i * dx, -1.1 + j * dx, -1.1 + k * dx, -1.1 + i * dx + dx, -1.1 + j * dx + dx, -1.1 + k * dx + dx);
            area += Q.m_qrule.sumWeights();
        }
        EXPECT_TRUE(std::abs(area - 4.0 *  3.1415926535897) < 1e-4);
    }

}