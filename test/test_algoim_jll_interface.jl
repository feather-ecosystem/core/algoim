module TestAlgoim

using Test
using Cxx
using Algoim

# initialize cpp-julia interface variables
include(joinpath(dirname(pathof(Algoim)), "setup.jl"))

# crate and arc
cxx"""
struct Ellips
{
    template<typename T>
    T operator() (const blitz::TinyVector<T, 2>& x) const{
        return x(0) * x(0) + 4.0 * x(1) * x(1) - 1.0;
    }

    template<typename T>
    blitz::TinyVector<T, 2> grad(const blitz::TinyVector<T, 2>& x) const{
        return blitz::TinyVector<T, 2>(2.0 * x(0), 8.0 * x(1));
    }
};
"""

cxx"""
struct UnitCircle
{
    template<typename T>
    T operator() (const blitz::TinyVector<T, 2>& x) const
    {
        return x(0) * x(0) + x(1) * x(1) - 1.0;
    }

    template<typename T>
    blitz::TinyVector<T, 2> grad(const blitz::TinyVector<T, 2>& x) const
    {
        return blitz::TinyVector<T, 2>(2.0 * x(0), 2.0 * x(1));
    }
};
"""

cxx"""
struct Sphere
{
    template<typename T>
    T operator() (const blitz::TinyVector<T, 3>& x) const
    {
        return x(0) * x(0) + x(1) * x(1) + x(2) * x(2) - 1.0;
    }

    template<typename T>
    blitz::TinyVector<T, 3> grad(const blitz::TinyVector<T, 3>& x) const
    {
        return blitz::TinyVector<T, 3>(2.0 * x(0), 2.0 * x(1), 2.0 * x(2));
    }
};
"""

struct Interval
    a::Float64
    b::Float64
end

@testset "volumetric quadrature properties" begin
    map = AlgoimMapping{2}(icxx"Ellips();")
    qr = AlgoimQuadRule(map, Interval(0,1), Interval(0,1); order=3)
    @test length(qr) == 45
    @test size(qr) == (45,)
    @test all(qr.w.>0.0)
end

@testset "ellipse 2d area fixed mesh" begin
    map = AlgoimMapping{2}(icxx"Ellips();")
    n = 16
    dx = dy = 2.2 / n
    area = 0.0

    for y in -1.1:dy:1.1
        for x in -1.1:dx:1.1
            qr = AlgoimQuadRule(map, Interval(x, x+dx), Interval(y, y+dy); order=4)
            area += sum(qr.w)
        end
    end
    @test abs(area - pi/2) < 1e-8
end

@testset "sphere 3d volume fixed mesh" begin
    map = AlgoimMapping{3}(icxx"Sphere();")
    n = 16
    dx = dy = dz = 2.2 / n
    volume = 0.0
    for z in -1.1:dz:1.1
        for y in -1.1:dy:1.1
            for x in -1.1:dx:1.1
                qr = AlgoimQuadRule(map, Interval(x, x+dx), Interval(y, y+dy), Interval(z, z+dz); order=4)
                volume += sum(qr.w)
            end
        end
    end
    @test abs(volume - (4/3) * pi) < 1e-8
end

@testset "sphere surface area fixed mesh" begin
    map = AlgoimMapping{3}(icxx"Sphere();")
    n = 16
    dx = dy = dz = 2.2 / n
    area = 0.0
    for z in -1.1:dz:1.1
        for y in -1.1:dy:1.1
            for x in -1.1:dx:1.1
                qr = AlgoimQuadRule(map, Interval(x, x+dx), Interval(y, y+dy), Interval(z, z+dz); order=4, dim=3)
                area += sum(qr.w)
            end
        end
    end
    @test abs(area - 4* pi) < 1e-8
end

@testset "sphere surface area side fixed mesh" begin
    map = AlgoimMapping{3}(icxx"Sphere();")
    n = 16
    dx = dy = dz = 2.2 / n
    area = 0.0
    x = 0
    for z in -1.1:dz:1.1
        for y in -1.1:dy:1.1
            qr = AlgoimQuadRule(map, Interval(x, x+dx), Interval(y, y+dy), Interval(z, z+dz); order=4, dim=0, side=0)
            area += sum(qr.w)
        end
    end
    @test abs(area - pi) < 1e-8
end

@testset "circle perimeter fixed mesh" begin
    map = AlgoimMapping{2}(icxx"UnitCircle();")
    n = 16
    dx = dy = 2.2 / n
    len = 0.0

    for y in -1.1:dy:1.1
        for x in -1.1:dx:1.1
            qr = AlgoimQuadRule(map, Interval(x, x+dx), Interval(y, y+dy); order=4, dim=2)
            len += sum(qr.w)
        end
    end
    @test abs(len - 2*pi) < 1e-8
end

@testset "circle side fixed mesh" begin
    map = AlgoimMapping{2}(icxx"UnitCircle();")
    n = 16
    dx = dy = 2.2 / n
    len = 0.0

    for y in 0:dy:1.1
        qr = AlgoimQuadRule(map, Interval(0, dx), Interval(y, y+dy); order=4, dim=0, side=0)
        len += sum(qr.w)
    end
    @test abs(len - 1) < 1e-8
end

end